package Repository_package;

import java.io.IOException;
import java.util.ArrayList;

import CommonUtility_method.Excel_data_extractor;

public class RPatch_Request {
	public static String RPatch_request_Tc2() throws IOException {
		ArrayList<String> Data = Excel_data_extractor.Excel_data_reader("TestData", "Patch_API", "patch_TC3");
				
		String name = Data.get(1);
		String job = Data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";
		return requestBody;

}
}
