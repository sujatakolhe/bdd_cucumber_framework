package TestDelivery_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPut_Request;
import Repository_package.RPut_request;
import io.restassured.path.json.JsonPath;

public class TCDPut_class extends Common_method_handle_API {
	@Test
	public static void Executor() throws IOException {
		File log_dir=Dir_Handle.create_DirLog("TCDPut_class");

		String put_requestBody = RPut_request . RPut_request_Tc2();
		String put_endpoint =EPut_Request .Eput_Request_tc2() ;
		for(int i=0 ; i<5 ;i++) {			
		
		int put_statuscode = put_statuscode(put_requestBody, put_endpoint);
	    System.out.println(put_statuscode);
	    if(put_statuscode==201) {
	    
	    String put_responseBody = put_responsebody(put_requestBody, put_endpoint);
	    System.out.println(put_responseBody);
	    Api_logs_handle.evidence_creator(log_dir,"TCDPut_class",put_endpoint,put_requestBody,put_responseBody);
		TCDPut_class.validator(put_requestBody ,put_responseBody);
	    break;
	    
	}
	    else {
	    	System.out.println("if status code is not valid then retry");
	    }
	    	
	    }	

	}

	public static void validator(String requestBody, String responsebody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		//System.out.println(jsp_res);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updateAt = jsp_res.getString("createdAt");
		System.out.println(res_updateAt);
		res_updateAt = res_updateAt.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updateAt, expected_date);
	}

}
