package TestDelivery_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPatch_Request;
import Repository_package.RPatch_Request;
import io.restassured.path.json.JsonPath;

public class TCDPatch_class extends Common_method_handle_API {
	@Test

	public static void Executor() throws IOException {
		File log_dir = Dir_Handle.create_DirLog("TCDPatch_class");
		String patch_requestBody = RPatch_Request.RPatch_request_Tc2();
		String patch_endpoint = EPatch_Request.EPpatch_endpoint_tc3();
		for (int i = 0; i < 5; i++) {
			int patch_statuscode = patch_statuscode(patch_requestBody, patch_endpoint);
			System.out.println(patch_statuscode);
			if (patch_statuscode == 201) {
				String patch_responseBody = patch_responsebody(patch_requestBody, patch_endpoint);
				System.out.println(patch_responseBody);
				Api_logs_handle.evidence_creator(log_dir, " TCDPatch_class", patch_endpoint, patch_requestBody,
						patch_responseBody);
				TCDPatch_class.validator(patch_requestBody, patch_responseBody);
				break;

			} else {
				System.out.println("retry when status code is not valid");
			}
		}

	}

	public static void validator(String requestbody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		// System.out.println(jsp_res);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updateAt = jsp_res.getString("createdAt");
		// System.out.println(res_updateAt);
		res_updateAt = res_updateAt.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updateAt, expected_date);

	}

}
