package cucumber.Options;
import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith (Cucumber.class)
@CucumberOptions(features="src/test/java/features",glue={"stepDefinition"},tags = "@Post_API_TestCases or @Put_API_TestCases ") 
public class TestRunner {
	
	}


