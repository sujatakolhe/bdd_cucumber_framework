package stepDefinition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EGet_request;
import TestDelivery_package.TCDGet_class;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Get_StepDefinition {

	int statuscode;
	String responseBody;
	File logdir;
	String Endpoint;

	@Given("Enter get endpoint")
	public void enter_get_endpoint() {
		logdir = Dir_Handle.create_DirLog("TCDGet_class");
		Endpoint = EGet_request.EGet_request_tc4();
	}

	@When("Send the get Api request")
	public void send_the_get_api_request() {
		statuscode = Common_method_handle_API.get_statuscode(Endpoint);
		responseBody = Common_method_handle_API.get_responsebody(Endpoint);
		System.out.println(responseBody);
	}

	@Then("validate get status code")
	public void validate_get_status_code() {
		Assert.assertEquals(statuscode, 200);

	}

	@Then("validate get response body parameters")
	public void validate_get_response_body_parameters() throws IOException {

		TCDGet_class.get_validator(responseBody);
		Api_logs_handle.evidence_creator(logdir, "TCDGet_class", Endpoint, responseBody);
		System.out.println("Get_API response validated successfully /n");

	}
}
