package stepDefinition;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPostRequest_class;
import Repository_package.RPost_Requset;
import TestDelivery_package.TCDPost_class;

import java.io.File;
import java.io.IOException;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Post_StepDefinition extends Common_method_handle_API {
	String requestBody;
	int statuscode;
	String responseBody;
	File logdir;
	String Endpoint;

	@Given("Enter NAME and JOB in post request body")
	public void enter_name_and_job_in_post_request_body() throws IOException {
		logdir= Dir_Handle.create_DirLog("TestCaseDelivery_class");
		Endpoint=EPostRequest_class.EPostRequest_class_tc1();
		requestBody=RPost_Requset.RPost_request_Tc2();
			}

	@When("Send the post request with payload")
	public void send_the_post_request_with_payload() {
		statuscode = Common_method_handle_API.post_statuscode(requestBody, Endpoint);
		responseBody = Common_method_handle_API.post_responsebody(requestBody, Endpoint);
		System.out.println(responseBody);
			}

	@Then("validate post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(statuscode, 201);
			}

	@Then("validate post response body parameters")
	public void validate_post_response_body_parameters() throws IOException {
		TCDPost_class.validator(requestBody, responseBody);
		Api_logs_handle.evidence_creator(logdir,"TestCaseDelivery_class", Endpoint, requestBody, responseBody);	
		
	       }

}
