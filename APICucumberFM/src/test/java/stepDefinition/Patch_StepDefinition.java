package stepDefinition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPatch_Request;
import Repository_package.RPatch_Request;
import TestDelivery_package.TCDPatch_class;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Patch_StepDefinition {
	String requestbody;
	String responsebody;
	String endpoint;
	int statuscode;
	File logDir;

	@Given("Enter NAME and JOB in patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		logDir = Dir_Handle.create_DirLog("TCDPatch_class");
		endpoint = EPatch_Request.EPpatch_endpoint_tc3();
		requestbody = RPatch_Request.RPatch_request_Tc2();
	}

	@When("Send the patch request with payload")
	public void send_the_patch_request_with_payload() {
		responsebody = Common_method_handle_API.patch_responsebody(requestbody, endpoint);
		statuscode = Common_method_handle_API.patch_statuscode(requestbody, endpoint);
		System.out.println(responsebody);
	}

	@Then("validate patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statuscode, 201);

	}

	@Then("validate patch response body parameters")
	public void validate_patch_response_body_parameters() throws IOException {
		TCDPatch_class.validator(requestbody, responsebody);
		Api_logs_handle.evidence_creator(logDir, "TCDPatch_class", endpoint, requestbody, responsebody);
		System.out.println(" patch_API response validated successfully \n");
	}

}
