package stepDefinition;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPut_Request;
import Repository_package.RPut_request;
import TestDelivery_package.TCDPut_class;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Put_StepDefinition {
	String requestbody;
	String responsebody;
	String endpoint;
	int statuscode;
	File logDir;
	@Before()
	public void setup() {
		System.out.println("Triggering Procces start here : ");		
	}
	@Given("Enter NAME and JOB in put request body")
	public void enter_name_and_job_in_put_request_body() throws IOException {
		logDir = Dir_Handle.create_DirLog("TCDPut_class");
		endpoint = EPut_Request.Eput_Request_tc2();
		requestbody = RPut_request.RPut_request_Tc2();
	}
	@When("Send the put request with payload")
	public void send_the_put_request_with_payload() throws IOException {
		responsebody = Common_method_handle_API.put_responsebody(requestbody, endpoint);
		statuscode = Common_method_handle_API.put_statuscode(requestbody, endpoint);
		System.out.println(responsebody);
		Api_logs_handle.evidence_creator(logDir, "TCDPut_class", endpoint, requestbody, responsebody);
	}
	@Then("validate put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(statuscode, 201);
	}
	@Then("validate put response body parameters")
	public void validate_put_response_body_parameters() throws IOException {
		TCDPut_class.validator(requestbody, responsebody);
		
		System.out.println(" put_API response validated successfully \n");
	}
		//Hooks
		@After
		public void teardown() {
			System.out.println("Process Ended here. ");
	}
	}
