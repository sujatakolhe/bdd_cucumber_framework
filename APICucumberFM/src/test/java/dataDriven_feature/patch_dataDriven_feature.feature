Feature: Trigger patch_dataDriven_API

Scenario Outline: Trigger the patch_Api Request with valid request parameters
          Given Enter "<name>" and "<job>" in patch request body
          When Send the patch request with payload
          Then validate patch status code
          And validate patch response body parameters
 
 Examples: 
         |name |job |        
         |pravin|Manager|
         |Shweta|sr.QA|
         |sujay|QA|