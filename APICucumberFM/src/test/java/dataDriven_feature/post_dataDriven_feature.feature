Feature: Trigger post_dataDriven_API

Scenario Outline: Trigger the post_Api Request with valid request parameters
          Given Enter "<name>" and "<job>" in post request body
          When Send the post request with payload
          Then validate post status code
          And validate post response body parameters
 
  Examples:
         |name |job |        
         |Sujata|QA|
         |Gaurav|fintech|
         |sagar|devops|

