package dataDriven_stepDefinition;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPostRequest_class;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class post_dataDriven_stepDefinition {
	String requestBody;
	int statuscode;
	String responseBody;
	File logdir;
	String Endpoint;

@Given("Enter {string} and {string} in post request body")
public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
	logdir= Dir_Handle.create_DirLog("TestCaseDelivery_class");
	Endpoint=EPostRequest_class.EPostRequest_class_tc1();
	 requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \"" +req_job + "\"\r\n" + "}";
    
}
@When("Send the post request with payload")
public void send_the_post_request_with_payload() throws IOException {
	statuscode = Common_method_handle_API.post_statuscode(requestBody, Endpoint);
	responseBody = Common_method_handle_API.post_responsebody(requestBody, Endpoint);
	System.out.println(responseBody);
	Api_logs_handle.evidence_creator(logdir,"TestCaseDelivery_class", Endpoint, requestBody, responseBody);	
}
@Then("validate post status code")
public void validate_post_status_code() {
	Assert.assertEquals(statuscode, 201);
}
@Then("validate post response body parameters")
public void validate_post_response_body_parameters() throws IOException {
	JsonPath jsp_req = new JsonPath(requestBody);
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");
	
	JsonPath jsp_res = new JsonPath(responseBody);
	String res_name = jsp_res.getString("name");
	String res_job = jsp_res.getString("job");
	
	Assert.assertEquals(res_name, req_name);
	Assert.assertEquals(res_job, req_job);
	System.out.println("post_API response body validate successful");
}

}
