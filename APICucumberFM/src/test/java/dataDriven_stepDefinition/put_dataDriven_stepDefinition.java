package dataDriven_stepDefinition;
import java.io.File;
import java.io.IOException;
import org.testng.Assert;
import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPut_Request;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
public class put_dataDriven_stepDefinition {
	String requestbody;
	String responsebody;
	String endpoint;
	int statuscode;
	File logDir;
@Given("Enter {string} and {string} in put request body")
public void enter_and_in_put_request_body(String req_name, String req_job) throws IOException {
	logDir = Dir_Handle.create_DirLog("TCDPut_class");
	endpoint = EPut_Request.Eput_Request_tc2();
	requestbody ="{\r\n" + "    \"Name\": \""+req_name+"\",\r\n" + "    \"Job\": \"" + req_job + "\"\r\n" + "}";
}
@When("Send the put request with payload")
public void send_the_put_request_with_payload() throws IOException {
	responsebody = Common_method_handle_API.put_responsebody(requestbody, endpoint);
	statuscode = Common_method_handle_API.put_statuscode(requestbody, endpoint);
	System.out.println(responsebody);
	Api_logs_handle.evidence_creator(logDir, "TCDPut_class", endpoint, requestbody, responsebody);
}
@Then("validate put status code")
public void validate_put_status_code() {
	Assert.assertEquals(statuscode, 201);
}
@Then("validate put response body parameters")
public void validate_put_response_body_parameters() throws IOException {
	JsonPath jsp_req = new JsonPath(requestbody);
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");	
	JsonPath jsp_res = new JsonPath(responsebody);
	String res_name = jsp_res.getString("name");
	String res_job = jsp_res.getString("job");
	Assert.assertEquals(res_name, req_name);
	Assert.assertEquals(res_job, req_job);	
	System.out.println("put_API response body validation successfull ");
}
}


