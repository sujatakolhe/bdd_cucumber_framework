package dataDriven_stepDefinition;
import java.io.File;
import java.io.IOException;
import org.testng.Assert;
import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EPatch_Request;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class patch_dataDriven_StepDefinition {
	String requestbody;
	String responsebody;
	String endpoint;
	int statuscode;
	File logDir;	
	@Given("Enter {string} and {string} in patch request body")
	public void enter_and_in_patch_request_body(String req_name, String req_job) throws IOException {
		logDir = Dir_Handle.create_DirLog("TCDPatch_class");
		endpoint = EPatch_Request.EPpatch_endpoint_tc3();
		requestbody ="{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n" + "}";
	}
	@When("Send the patch request with payload")
	public void send_the_patch_request_with_payload() throws IOException {
		responsebody = Common_method_handle_API.patch_responsebody(requestbody, endpoint);
		statuscode = Common_method_handle_API.patch_statuscode(requestbody, endpoint);
		System.out.println(responsebody);
		Api_logs_handle.evidence_creator(logDir, "TCDpatch_class", endpoint, requestbody, responsebody);
	}
	@Then("validate patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statuscode, 201);
	}
	@Then("validate patch response body parameters")
	public void validate_patch_response_body_parameters() throws IOException {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");		
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);		
		System.out.println("patch_API response body validation successfull ");
	}
	}



	


