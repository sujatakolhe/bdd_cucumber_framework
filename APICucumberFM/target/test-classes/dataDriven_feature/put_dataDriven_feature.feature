Feature: Trigger put_dataDriven_API

Scenario Outline: Trigger the put_Api Request with valid request parameters
          Given Enter "<Name>" and "<Job>" in put request body
          When Send the put request with payload
          Then validate put status code
          And validate put response body parameters
 
 Examples: 
         |Name |Job |        
         |sonali|Lecture|
         |sonam|Teacher|
         |megha|CA|