Feature: Trigger patch_Api
@Patch_API_TestCases
Scenario: Trigger the patch_Api Request with valid request parameters
          Given Enter NAME and JOB in patch request body
          When Send the patch request with payload
          Then validate patch status code
          And validate patch response body parameters
          