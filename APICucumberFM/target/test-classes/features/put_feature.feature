Feature: Trigger put_Api
@Put_API_TestCases
Scenario: Trigger the put_Api Request with valid request parameters
          Given Enter NAME and JOB in put request body
          When Send the put request with payload
          Then validate put status code
          And validate put response body parameters